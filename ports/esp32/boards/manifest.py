freeze(
    "$(PORT_DIR)/modules",
    (
        "urequests/__init__.py",
        "_boot.py",
        "errno.py",
        "ffilib.py",
        "flashbdev.py",
        "inisetup.py",
        "stat.py",
        "tm1637.py",
        "uuid.py"
    )
    , opt=3
)
freeze("$(MPY_DIR)/ports/esp8266/modules", "ntptime.py", opt=3)
include("$(MPY_DIR)/extmod/uasyncio/manifest.py")

freeze("$(MPY_DIR)/micropython-lib/python-stdlib/datetime", "datetime.py", opt=3)
freeze("$(MPY_DIR)/micropython-lib/python-stdlib/logging", "logging.py", opt=3)
freeze(
    "$(MPY_DIR)/tinyweb",
    (
        "tinyweb/__init__.py",
        "tinyweb/server.py",
    )
    , opt=3
)
freeze("$(MPY_DIR)/MicroDNSSrv", "microDNSSrv.py", opt=3)
