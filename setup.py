from os import remove, rmdir
import upip

packages = [
    "micropython-tm1637==1.3.0",
    "micropython-uuid==0.1",
    "micropython-urequests==0.9.1",
]

upip.index_urls = ["https://pypi.org/pypi"]
upip.install(packages, "ports/esp32/modules")

remove("./ports/esp32/modules/os/__init__.py")
remove("./ports/esp32/modules/os/path.py")
rmdir("./ports/esp32/modules/os")
